import { Text as TextView } from "react-native";
import React from "react";
import { Fonts } from "../../assets/styles/index";

const Text = ({
    children,
    fontSize = 14,
    color = 'black',
    regular = true,
    semibold = false,
    bold = false,
    style,
    ...props

}) => {
    return(
        <TextView
            {...props}
            style = {[
                style,
                { fontSize: fontSize, color: color, lineHeight: fontSize * 1.5},
                regular && { fontFamily: Fonts.Nunito.Regular},
                semibold && { fontFamily: Fonts.Nunito.SemiBold},
                bold && { fontFamily: Fonts.Nunito.Bold},
            ]}
            >
        {children}
        </TextView>
        
    )
}

export default Text;