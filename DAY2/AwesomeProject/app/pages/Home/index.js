import { View, Text, TextInput } from 'react-native'
import React from 'react'
import { HomeIcon, Box } from '../../assets/svg'
import { HEIGHT, WIDTH } from '../../assets/styles'
import { Fonts } from '../../assets/styles'

export default function Home() {
  return (
    <View style={{paddingTop: 54, paddingHorizontal: 16, height: HEIGHT, backgroundColor: '#FFF'}}>
      <View style={{flexDirection: 'row'}}>
        <Box WIDTH={52} HEIGHT={52} fill={'#A7A7A7'} marginRight={16}></Box>
        <TextInput 
          style={{
            backgroundColor: '#FBFBFB', 
            height: 52, 
            flex: 1, 
            borderRadius: 80,
            paddingLeft: 20,
            fontFamily: Fonts.Nunito.Bold,
            }}
          placeholder='Find Your Projects'
          placeholderTextColor='#A7A7A7'

          ></TextInput>
      </View>
    </View>

  )
}