import { View, Text } from 'react-native'
import React from 'react'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import Home from '../Home';
import Profile from '../profile';
import Performance from '../Performance';
import { HomeIcon, ProfileIcon, TaskIcon, PerformIcon, BgPlus, Add, AddLead } from '../../assets/svg';
import Task from '../Task';
import Plus from '../Plus';
import { Fonts } from '../../assets/styles';

const Tab = createBottomTabNavigator();

export default function Main({ navigation, router }) {
  return (
    <View style={{ flex: 1 }}>
      <Tab.Navigator
        screenOptions={{
          headerShown: false,
          tabBarStyle: {
            height: 65,
            paddingRight: 16,
            paddingLeft: 16,
            paddingBottom: 11,
            paddingTop: 11
          },
          tabBarActiveTintColor: '#04325F',
          tabBarLabelStyle: {
            fontSize: 10,
            lineHeight: 17,
            fontWeight: 700,
            fontFamily: Fonts.Nunito.Regular,
          },
        }}
      >
        <Tab.Screen
          name='Home'
          component={Home}
          options={{
            tabBarIcon: ({ focused }) => <HomeIcon fill={focused ? '#04325F' : '#CED1D4'} />
          }}
        />
        <Tab.Screen
          name='Task'
          component={Task}
          options={{
            tabBarIcon: ({ focused }) => (
              <TaskIcon fill={focused ? '#04325F' : '#CED1D4'} />
            ),
          }}
        />
        <Tab.Screen
          name='Plus'
          component={Plus}
          options={{
            tabBarIcon: () => (
              <AddLead></AddLead>
            ),
            tabBarLabel: ''
          }}
        />
        <Tab.Screen
          name='Performance'
          component={Performance}
          options={{
            tabBarIcon: ({ focused }) => (
              <PerformIcon fill={focused ? '#04325F' : '#CED1D4'} />
            )
          }}
        />
        <Tab.Screen
          name='Profile'
          component={Profile}
          options={{
            tabBarIcon: ({ focused }) => (
              <ProfileIcon fill={focused ? '#04325F' : '#CED1D4'} />
            )
          }}
        />
      </Tab.Navigator>
    </View>
  )
}