import { View, ImageBackground, TextInput, TouchableOpacity } from 'react-native'
import React, { useEffect, useState } from 'react'
import image from '../../assets/img/index'
import { Fonts, HEIGHT, WIDTH } from '../../assets/styles'
import Text from '../../components/Text'
import { Eye, EyeSlash } from '../../assets/svg'
import Satellite from '../../services/satellite'
import { ActivityIndicator } from 'react-native-paper'

export default function Login({ router, navigation }) {
    const [isEnable, setIsEnable] = useState(true);
    const [showPass, setShowPass] = useState(true);
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");

    const [errorEmail, setErrorEmail] = useState("");
    const [errorPassword, setErrorPassword] = useState("");

    const [isLoading, setIsLoading] = useState(false);

    useEffect(()=>{

        if(email !== "" && password !== "" && errorEmail === "" &&  errorPassword ===""        
        ){
            setIsEnable(false)
        }else{
            setIsEnable(true)
        }
    }, [email, password, errorEmail, errorPassword]);

    const onSubmit = async () => {
        setIsLoading(true);
        
        const body = {
            email : email,
            password : password
        };
        console.log("BODY", JSON.stringify(body, null, 2))
        

        // try{
        //     const response = await Satellite.post("auth/login", body)
        //     // return response;
        //     console.log("Response", response.data)
        // }catch(err){
        //     console.log(err);
        // }finally{
        //     console.log("Finally")
        // }
        Satellite.post("auth/login", body)
        .then((response) => {
            console.log("Response", response.data)
            navigation.navigate("Main")
        })
        .catch((error) => {
            console.log("Error", error)
            setErrorPassword("Invalid Email or Password")
        })
        .finally(() => {
            setTimeout(()=>{
                setIsLoading(false);
            }, 1000)
            
        })
        
    }

    
    return (
        <ImageBackground
            source={image.BG_LOGIN}
            style={{ width: WIDTH, height: HEIGHT }}
            resizeMode='cover'
        >
            <View style={{ marginTop: 90, marginHorizontal: 16 }}>
                <View>
                    <Text color='#FFF' fontSize={16} bold>Email</Text>
                </View>
                <View style={{
                    borderWidth: 1,
                    marginTop: 8,
                    borderColor: errorEmail ? '#EA8685' : '#132040',
                    borderRadius: 8,
                    padding: 12,
                    backgroundColor: '#273C75'
                }}>
                    <TextInput
                        keyboardType='email-address'
                        placeholder={'Enter Your Email'}
                        placeholderTextColor={'#D3D3D3'}
                        style={{
                            color: '#FFF',
                            fontFamily: Fonts.Nunito.Bold,
                            fontSize: 16
                        }}
                        onChangeText={(value) => {
                            setEmail(value);
                            const emailRegex =
                                /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
                            if (value === "") {
                                setErrorEmail("email must be filled in");
                                return;
                            }
                            if (!emailRegex.test(value)) {
                                setErrorEmail("Invalid Mail Address")
                                return;
                            }
                            setErrorEmail("");
                        }} />
                </View>
                <Text color='#EA8685' style={{ textAlign: "right", marginRight: 8 }}>{errorEmail}</Text>
            </View>
            <View style={{ marginTop: 27, marginHorizontal: 16 }}>
                <View>
                    <Text color='#FFF' fontSize={16} bold>Password</Text>
                </View>
                <View style={{
                    borderWidth: 1,
                    marginTop: 8,
                    borderColor: errorPassword ? '#EA8685' : '#132040',
                    borderRadius: 8,
                    padding: 12,
                    backgroundColor: '#273C75',
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    alignItems: 'center'
                }}>
                    <TextInput
                        secureTextEntry={showPass}
                        placeholder={'Enter Your Password'}
                        placeholderTextColor={'#D3D3D3'}
                        maxLength={12}
                        style={{
                            color: '#FFF',
                            fontFamily: Fonts.Nunito.Bold,
                            fontSize: 16,
                            width: 400,
                        }}
                        onChangeText={(value) => {
                            setPassword(value);
                            if (value === "") {
                                setErrorPassword("password must be filled in")
                                return;
                            }

                            setErrorPassword("")
                        }}

                    />

                    <TouchableOpacity onPress={() => setShowPass(!showPass)}>
                        {showPass ? (
                            <Eye width={20} height={20} stroke={'#FFF'}></Eye>
                        ) : (<EyeSlash width={20} height={20} stroke={'#FFF'}></EyeSlash>)}

                    </TouchableOpacity>
                </View>
                <View style={{flexDirection: 'row'}}>
                    <Text color='#F6E58D' fontSize={10} style={{ marginTop: 5, marginLeft: 5 }}>
                        Forgot Password?
                    </Text>

                    <Text color='#EA8685' style={{flex: 1, textAlign: 'right', marginRight: 8}}>{errorPassword}</Text>
                </View>


                <TouchableOpacity
                    disabled={isEnable || isLoading}
                    onPress= {onSubmit}
                    style={{
                        backgroundColor: '#18DCFF',
                        borderRadius: 8,
                        paddingVertical: 12,
                        alignItems: 'center',
                        marginTop: 20,
                        opacity: isEnable || isLoading ? 0.5 : 1
                    }}

                >
                    {isLoading?(
                        <ActivityIndicator size={'small'} color='#261A31'></ActivityIndicator>
                    ):(
                        <Text color='#261A31' fontSize={16} bold>Login</Text>
                    )}
                </TouchableOpacity>
                <View style={{
                    alignItems: 'center',
                    marginTop: 20
                }}>
                    <Text bold color='#FFF'>
                        Dont Have an Account?{" "}
                        <Text bold color='#F6E58D' onPress={() => navigation.navigate("Register")}>
                            Sign Up
                        </Text>
                    </Text>
                </View>
            </View>
        </ImageBackground>
    )
}