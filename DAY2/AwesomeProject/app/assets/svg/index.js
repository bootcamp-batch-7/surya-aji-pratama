import HomeIcon from "./home.svg"
import ProfileIcon from './profile.svg'
import TaskIcon from './task.svg'
import PerformIcon from './perform.svg'
import BgPlus from './Bg.svg'
import Add from './Add.svg'
import Eye from './eye.svg'
import EyeSlash from './eyeSlash.svg'
import ArrowBack from './arrow_back.svg'
import AddLead from './addLeads.svg'
import Box from './box.svg'

export{
    HomeIcon, ProfileIcon, TaskIcon, PerformIcon, BgPlus, Add, Eye, EyeSlash, ArrowBack, AddLead, Box
}