import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, Image, ScrollView, TextInput, Button } from 'react-native';
import { useState, Component } from 'react';
export default function App() {
  const [text, setText] = useState("");
  const [count, setCount] = useState(0);
  return (
    <View style={styles.container}>
      <Text>Test</Text>
      <Image source={require("./assets/favicon.png")}></Image>
      <View style={{ flexDirection: 'row', borderWidth: 1 }}>
        <Image
          style={{ width: 100, height: 100 }}
          source={{ uri: "https://img.icons8.com/?size=512&id=tSsoqhIYwMTk&format=png", }}
          resizeMode='center'
        />
        <Image
          style={{ width: 100, height: 100 }}
          source={{ uri: "https://img.icons8.com/?size=512&id=tSsoqhIYwMTk&format=png", }}
          resizeMode='center'
        />
      </View>
      <View style={{borderRadius: 100}}>
        <TextInput
          style={{ borderWidth: 1, marginTop: 100, width: 200 }}
          onChangeText={(value) => {
            console.log(value);
            setText(value);
          }}
        />
      </View>
      <Text style={{ color: "red", fontSize: 36 }}>{text}</Text>
      <Text style={{ color: "red", fontSize: 36 }}>{count}</Text>
      <AppClass cobaText={text} />
      <AppFunction
        cobaText={text}
        buttonAction={(v) => setCount(count + v)}
      />
      <StatusBar style="auto" />
    </View>
  );
}

class AppClass extends Component {
  render() {
    return (
      <View>
        <Text>Haloo</Text>
        <Text>{this.props.cobaText}</Text>
      </View>
    );
  }
}

const AppFunction = ({ cobaText, buttonAction }) => {
  return (
    <View>
      <Text>INI DARI FUNCTION</Text>
      <Text>{cobaText}</Text>
      <Button title='Ini Button' onPress={() => buttonAction(5)} />
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'center'
  },
});
