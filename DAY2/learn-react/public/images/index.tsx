const images = {
    LOGO_BERIJALAN: '/images/logo.png',
    SUN: '/images/sun.png',
    MOON: '/images/moon.png'
}

export default images