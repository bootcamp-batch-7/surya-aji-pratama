import axios from "axios";

const MOVIEDB_API_KEY = 'eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiJmN2ZiOTUzNWEzMzY0ZDAxODBiMTM0YmI1YWM0Yzk2MSIsInN1YiI6IjY0YmE2YzMwMDZmOTg0MDBmZTQyMjM5OSIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ._3vBf0qB0NAwJtKRiFUgYTPDK2tNx28Qc2N-XiwlD0M'; // Replace this with your actual GitHub API key

const movieDBConfig = {
  baseURL: 'https://api.themoviedb.org/3',
  timeout: 10000,
  headers: {
    Authorization: `Bearer ${MOVIEDB_API_KEY}`,
  },
};

const githubConfig = {
  baseURL: 'https://api.github.com',
  timeout: 10000,
};

const MovieDBSatellite = axios.create(movieDBConfig);
const GitHubSatellite = axios.create(githubConfig);

export { MovieDBSatellite, GitHubSatellite };
