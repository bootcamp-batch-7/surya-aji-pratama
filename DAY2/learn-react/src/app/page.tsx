import { Container } from '@components'
import Image from 'next/image'

export default function Home() {
  return (
    <Container>
      <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', height: '100vh' }}>
        <h1 style={{ fontSize: 50, fontWeight: 'bold' }}>Home</h1>
      </div>

    </Container>

  )
}
