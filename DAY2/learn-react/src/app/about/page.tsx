import { Container } from '@components';
import React from 'react'


export const metadata = {
  title: "About Us"
};
export default function About() {
  return (
    <Container>
      <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', height: '100vh' }}>
        <h1 style={{ fontSize: 50, fontWeight: 'bold' }}>About</h1>
      </div>

    </Container>
  )
}
