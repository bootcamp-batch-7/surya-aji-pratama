import '@assets/styles/globals.css'
import { Header, Footer } from '@components'
import type { Metadata } from 'next'
import { Nunito } from 'next/font/google'

const inter = Nunito({ subsets: ['latin'] })

export const metadata: Metadata = {
  title:{
    default: "MyApp | Home",
    template: "MyApp | %s",
  },
}

export default function RootLayout({
  children,
}: {
  children: React.ReactNode
}) {
  return (
    <html lang="en">
      <body>
      <main className={`dark:text-white dark:bg-slate-900 ${inter.className}`}>
        <Header/>
          {children}
        <Footer/>
      </main>
      </body>
    </html>
  )
}
