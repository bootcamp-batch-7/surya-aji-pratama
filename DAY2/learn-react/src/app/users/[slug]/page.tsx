import { Container } from '@components'
import { GitHubSatellite } from '@services/satellite';
import Image from 'next/image';
import React from 'react'


async function getUserData(param: string) {
  let res;
  await GitHubSatellite.get('/users/' + param)
    .then((response) => {
      res = response.data;
    }).catch((error) => {
      console.log("error", error);
    });
  return res;
}

export default async function UserDetails({ params }: { params: { slug: string } }) {
  const data = await getUserData(params.slug) as any
  return (
    <Container>
      <div className='flex flex-col'>
        <h1>Ini user : {params.slug}</h1>
        <h1>ID: {data?.id}</h1>
        <h1>Github Link: {data?.url}</h1>
        <Image src={`${data?.avatar_url}`} alt='Profil' width={500} height={500}></Image>
      </div>
    </Container>

  )
}
