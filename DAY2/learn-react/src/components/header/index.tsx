"use client"; 
 
import Image from "next/image"; 
import Link from "next/link"; 
import images from "../../../public/images"; 
import { useEffect, useState } from "react";
import { usePathname } from "next/navigation";
import { GetStorage, SetStorage } from "@services/storage";
 
export default function Header() { 

  const pathName = usePathname();
  const localTheme = GetStorage("theme")


  const menuHeader = [ 
    { name: "Home", href: "/" }, 
    { name: "Galery", href: "/gallery" }, 
    { name: "About Us", href: "/about" }, 
    { name: "User", href: "/users" }, 
  ]; 
 
  const [isDark, setIsDark] = useState(localTheme === "dark");

  useEffect(()=>{
    const getCurrentTheme = () => 
        window.matchMedia("(prefers-color-scheme: dark)").matches;
        setIsDark(getCurrentTheme());
    }
  , [])
  
  useEffect(()=>{
    if(isDark){
        document.body.classList.add("dark");
    }else{
        document.body.classList.remove("dark");
    }

    SetStorage("theme", isDark ? "dark" : "light");
  }, [isDark])

  return ( 
    <header className="flex flex-row px-5 justify-between custom-shadow"> 
      <Link href={'/'} className="py-2 flex flex-row justify-center" style={{alignItems: "center"}}> 
        <Image 
          src={images.LOGO_BERIJALAN} 
          width={38} 
          height={45} 
          alt="logo-berijalan" 
        ></Image> 
        <h1 className="mx-2 ">Berijalan</h1> 
      </Link> 
 
      <ul className="flex flex-row"> 
        <div 
          className="p-5 " 
          style={{ cursor: "pointer" }} 
          onClick={() => setIsDark(!isDark)} 
        > 
          {isDark ? ( 
            <Image 
              src={images.SUN} 
              width={24} 
              height={24} 
              alt="dark mode" 
            ></Image> 
          ) : ( 
            <Image 
              src={images.MOON} 
              width={24} 
              height={24} 
              alt="light mode" 
            ></Image> 
          )} 
        </div> 
        {menuHeader.map((item, index) => ( 
          <Link key={index} href={item.href}> 
            <li className={`${pathName===item.href ? "header-selection" : ""} p-5 hover:bg-slate-500`}> 
              {item.name} 
            </li> 
          </Link> 
        ))} 
      </ul> 
    </header> 
  ); 
}